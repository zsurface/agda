open import Data.String
open import Function
open import IO.Primitive
 
main = readFiniteFile "/tmp/x.x" >>= putStr ∘ toCostring
