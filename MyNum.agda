-- {-# BUILTIN NATPLUS _+_ #-}
-- {-# BUILTIN NATTIMES _*_ #-}
-- {-# BUILTIN NATMINUS _-_ #-}


module MyNum where

{-# FOREIGN GHC import Data.Maybe #-}
{-# FOREIGN GHC
  data Foo = Foo | Bar Foo
  countBars :: Foo -> Integer
  countBars Foo = 0
  countBars (Bar f) = 1 + countBars f
#-}

{-|
 Last updated: 04-02-2020

 help:
 set input method
 (default-input-method "Agda")
 
 C-c C-x C-c GHC compile to binary file and run the binary file in shell
 C-c C-n  => Run function

 Save your time with monitor
 monitor 1 'agda --compile MyNum.agda ; ./MyNum'  MyNum.agda
-} 

-- Agda standard library 0.7
open import Data.Nat
open import Data.Nat.Show using (show) -- show 3
open import Data.Empty
open import Data.Unit.Base using (⊤; tt)
open import Data.Nat.Base

open import Data.Unit using (⊤)
open import Data.String using (toCostring; String; _++_)
open import Foreign.Haskell using (Unit)
open import Data.List using (reverse)
open import Data.String using(toList; fromList; String; toCostring)

-- open import Data.String as D
open import Agda.Builtin.IO
-- import IO.Primitive as P

open import Coinduction
open import Function

open import Function using (_∘_)
open import Foreign.Haskell


data MyBool : Set where
   mytrue : MyBool
   myfalse : MyBool

data MyNat : Set where
  mzero : MyNat
  mysucc : MyNat → MyNat

one = mysucc (mzero)
two = mysucc (mysucc (mzero))

_+++_ : MyNat → MyNat → MyNat
mzero +++ mzero = mzero
mzero +++ b  = b
(mysucc a) +++ b = mysucc (a +++ b)

-- 2^0 = 1
-- 2^1 = 2
-- 2^2 = 2*2
-- 2^3 = 2*2*2
-- 2^(2 + 1) = 2^2  x 2

_**_ : MyNat → MyNat → MyNat
mzero ** b = mzero
(mysucc a) ** b = (a ** b) +++ b

pow : MyNat -> MyNat -> MyNat
pow _ mzero = one
pow a (mysucc b) = (pow a b) ** a 

myeq : MyNat -> MyNat -> MyBool
myeq mzero mzero      = mytrue
myeq mzero (mysucc x) = myfalse
myeq (mysucc x) mzero = myfalse
myeq (mysucc a) (mysucc b) = myeq a b


mygt : MyNat -> MyNat -> MyBool
mygt mzero mzero = myfalse
mygt (mysucc x) mzero = mytrue
mygt mzero (mysucc x) = myfalse
mygt (mysucc a) (mysucc b) = mygt a b

mylt : MyNat -> MyNat -> MyBool
mylt mzero mzero = myfalse
mylt (mysucc x) mzero = myfalse
mylt mzero (mysucc x) = mytrue
mylt (mysucc a) (mysucc b) = mylt a b




{-
 Try to write mod and rem functions first
natToStr : MyNat -> String
natToStr mzero = "mzero"
natToStr one = "one"
natToStr (mysucc a) = "two"
-}




mynot : MyBool -> MyBool
mynot mytrue = myfalse
mynot myfalse = mytrue


infixr 4 _::_
data List(A : Set) : Set where
  [] : List A
  _::_ :  A → List A → List A


-- data Maybe a = Nothing | Just a

data Maybe (A : Set) : Set where
  nothing : Maybe A
  just : A -> Maybe A


length : {A : Set} -> List A -> ℕ
length [] = zero
length (x :: cs) = suc (length cs)


head : {A : Set} -> List A -> Maybe A
head [] = nothing
head (x :: cs) = just x



data Square : ℕ -> Set where
  sq : (m : ℕ) -> Square ( m * m )

root : (n : ℕ) -> Square n -> ℕ
root . (m * m) (sq m) = m

-- fun2 : {A : Set} -> A -> A
-- fun2 a = a

map : {A B : Set} -> (A -> B) -> List A -> List B
map f [] = []
map f (x :: cs) = f x :: map f cs

empty : List ℕ
empty = []

upto3 : List ℕ
upto3 = 3 :: 4 :: 5 :: []

f1 : ℕ -> ℕ
f1 a = a + 1


-- subtraction
_-_ : ℕ → ℕ → ℕ
x - zero = x
zero - suc x = zero
(suc x) - (suc y) = x - y

five : ℕ
five = 5
     
data Bool : Set where
  true : Bool
  false : Bool

not : Bool → Bool
not true = false
not false = true

if_then_else : { A : Set } -> Bool -> A -> A -> A
if true then x else y = x
if false then x else y = y


_lt_ : ℕ → ℕ → Bool
zero lt (suc n) = true
(suc m) lt (suc n) = m lt n
_ lt _ = false


_eq_ : ℕ → ℕ → Bool
zero eq zero = true
(suc m) eq (suc n) = m eq n
_ eq _ = false

_and_ : Bool → Bool → Bool
false and _ = false
_ and false = false
_ and _ = true

_or_ : Bool → Bool → Bool
true or _ = true
_ or true = true
_ or _ = false



-- 3
_lte_ : ℕ → ℕ → Bool
m lte n = (m lt n) or (m eq n)
 

{- \bN  -}
{-
_div_ : ℕ → ℕ → ℕ
_ div zero = 10000
zero div _ = 0
x div y = if (zero lte (x - y)) then (1 + ((x - y) div y)) else zero
-}

fun : ℕ → ℕ 
fun n = n + n

fib : ℕ → ℕ
fib 0 = 0
fib 1 = 1
fib (suc (suc n)) = fib (suc n) + fib n
    
_≡_ : ℕ → ℕ → Bool
zero ≡ zero = true
(suc m) ≡ (suc n) =  m ≡ n
_ ≡ _ = false

_≢_ : ℕ → ℕ → Bool
zero ≢ zero = false
(suc m) ≢ (suc n) = m ≢ n
_ ≢ _ = true


𝑇 : ℕ → ℕ → ℕ
𝑇 m n = n 

_σ_ : ℕ → ℕ → ℕ
zero σ m = m
suc m σ n = suc (m σ n)

_Σ_ : ℕ → ℕ → ℕ
zero Σ m = m
suc m Σ n = suc (m Σ n)

Σ[_,_] : ℕ → ℕ → ℕ
Σ[ n , h ] = if n ≡ 3 then n else h

_⊗_ : ℕ → ℕ → ℕ
m ⊗ n = m + n

_⊕_ : ℕ → ℕ → ℕ
m ⊕ n = m + n


{-
<_,_> : ℕ → ℕ → ℕ
< m , n > = m + n
-}

{- less than equal -}
_lee_ : ℕ → ℕ → Set
0 lee n = ⊤
(suc n) lee (suc m) = n lee m
(suc n) lee 0 = ⊥

f2 : ℕ → ℕ
f2 n = if n ≡ 3 then 3 else 4


main' : IO ⊤
main' = putStrLn (show (five + five))

{-
print : IO ⊤
print = putStrLn (show < 3 , 4 >)
-}

identity : ( A : Set) → A → A
identity = \A x → x

num : ℕ
num = 3

print2 : IO ⊤
print2 = putStrLn (show (3 ⊗ 3))

  




{- not working 
main : IO Unit
main =
  getLine >>= (\s → return (toCostring (fromList(reverse (toList s)))) >>= (\s' → putStrLn s'))
-}

{- C-c C-x C-c => GHC RET -}

{-
main = run do
       ♯ putStrLn ("dog" ++ "pig")
       ♯ do
         ♯ putStrLn "rat"
         ♯ do
           ♯ putStrLn "one"
           ♯ putStrLn "what"
-}



{- 
  chain IO in Agda 
  https://stackoverflow.com/questions/55184904/what-is-the-right-way-to-use-the-do-notation-with-agda-stdlibs-io
-}
{-
main = run do
       ♯ (♯ (♯ (putStrLn "dog") >> ♯ putStrLn "cat") >> ♯ putStrLn "cow" ) >> ♯ putStr "pig"
-}

{-
  write string to file
-}
{-
  main = run (writeFile "/tmp/ff.x" (show 2))
-}
{-
   main = run (putStrLn (show (root 4  (sq 3))))
-}


record Pair (A B : Set) : Set where
  constructor _,_
  field
    fst : A
    snd : B

p45 : Pair ℕ ℕ
p45 = 4 , 5

-- record type
record MyPair (A B : Set) : Set where
  field
    fst : A
    snd : B

open MyPair

-- Pair.fst : {A B : Set} -> Pair A B -> A
-- Pair.s : {A B : Set} -> Pair A B -> B
p23 : Pair ℕ ℕ
p23 = record { fst = 2; snd = 4}

-- Pairs.
record _∧_ (x y : Set) : Set where
  constructor _,_
  field
    fst : x
    snd : y

mypair : Pair ℕ ℕ     
Pair.fst mypair = 3
Pair.snd mypair = 4

pair2 : (ℕ ∧ ℕ)
pair2 = record {fst = 2; snd = 9}

-- space is matter in Agda  (4,4) => error
pair3 : (ℕ ∧ ℕ)
pair3 = (4 , 4) 

data Bin : Set where
  ζ : Bin
  _O : Bin -> Bin
  _I : Bin -> Bin

-- inc : Bin -> Bin
-- https://plfa.github.io/Naturals/
-- KEY: check this out to how to implement the nature number as binstring
-- inc x => from (to(x) + 1)
-- 110 + 1 = 11 + 10 ?
-- 111  = 111
-- 110 + 11 = 11 + 110
-- 101 + 10 = 10 + 101
-- 111        111
-- 100 + 11 = 10 + 110?
-- => 111         1000
-- a/2 + b*2
-- a + b
-- (a/2) + (a/2) + (b*2 - b)
--   x O => x + x
-- x1 O = x1 + x1
-- II + I =
--   x I = x O + I
-- inc (x I) = inc (x O) + I
-- inc (x I) = (x O) + inc (ζ I)
-- inc (x I) = (x O) + I O
-- inc (x I) = (x + I) O
-- inc (x I) = (inc x) O
-- inc (inc I O)
inc : Bin -> Bin
inc ζ = ζ
inc (ζ O) = ζ I
inc (ζ I) = (ζ I) O
inc ((x O) O) = (x O) I
inc ((x I) O) = (x I) I
inc (x I) = (inc x) O
-- inc (ζ I) = (ζ I) O
-- I I + I = I O O
binAdd : Bin -> Bin -> Bin
binAdd ζ a = a
binAdd (ζ O) (ζ I) = ζ I
binAdd (ζ I) (ζ O) = ζ I
binAdd (ζ I) (ζ I) = inc (ζ I)
binAdd (ζ O) a = a
binAdd (ζ I) a = inc a
binAdd a (ζ I) = inc a
binAdd (x O) x1 = binAdd x (binAdd x x1)
binAdd (x I) x1 = inc (binAdd x (binAdd x x1))

_beq_ : Bin -> Bin -> MyBool
ζ beq ζ = mytrue
(ζ O) beq (ζ O) = mytrue
(ζ I) beq (ζ I) = mytrue
(ζ O) beq (ζ I) = myfalse
(ζ I) beq (ζ O) = myfalse
(a O) beq (b I) = myfalse
(a I) beq (b O) = myfalse
(a O) beq (b O) = a beq b
(a I) beq (b I) = a beq b
ζ beq _ = myfalse
_ beq ζ = myfalse

b2Str : MyBool -> String
b2Str mytrue = "mytrue"
b2Str myfalse = "myfalse"

to : ℕ -> Bin
to 0 = ζ O
to 1 = ζ I
to (suc n) = inc (to n)

from : Bin -> ℕ
from ζ = 0
from (ζ O) = 0
from (ζ I) = 1
from (x O) = (from x) + (from x)
from ((x O) I) = (from (x O)) + (from (x O)) + 1
from ((x I) I) = (from (x I)) + (from (x I)) + 1

-- binAdd (inc x) (ζ O) = inc x
-- binAdd (inc x) (ζ I) = inc (inc x)
-- binAdd (inc x) x1 = binAdd x (inc x1)
-- binAdd (ζ O) a = a
-- binAdd (ζ b) (ζ O) = ζ b
-- binAdd (inc a) b = binAdd (binAdd a b) (ζ I) 

{-
from : Bin -> ℕ
from (ζ O) = 0
from (ζ I) = 1
-}

{-
to : ℕ -> Bin
to 0 = ζ O
to 1 = ζ I
to (n + 1) = inc n
-}


{-
main = run do
       ♯ (♯ (♯ (♯ (putStrLn (show (4 - 0)))
         >> ♯ putStrLn (show (fib 30)))
         >> ♯ putStrLn ("cow" ++ "cat") )
         >> ♯ putStr (show (Pair.fst mypair)))
         >> ♯ putStr "rat"
-}
{-
main = run do
       ♯ (putStrLn "dog") >> ♯ putStr "pig"
-}
{-main = run do
       ♯ (putStrLn "cat") >> ♯ (putStrLn "dog")
-}


main = run do
       ♯ (♯ (♯ (♯ (♯ (♯ (putStrLn "cat")
         >> ♯ putStrLn "dog")
         >> ♯ putStrLn (b2Str ((binAdd (ζ O) (ζ I)) beq (ζ I))))
         >> ♯ putStrLn (b2Str ((binAdd (ζ O I) (ζ O I)) beq (ζ I O))))
         >> ♯ putStrLn (b2Str ((binAdd (ζ O I) (ζ I I)) beq (ζ I O O))))
         >> ♯ putStrLn (b2Str ((to 3) beq (ζ I I))))
         >> ♯ putStrLn (b2Str ((to 63) beq (ζ I I I I I I)))

{-
   main = readFiniteFile "/tmp/x.x" >>=  putStr ∘ toCostring
-}
{-
main = readFiniteFile "data.txt" >>= putStr ∘ toCostring
-}

{-
main = (readFiniteFile "/tmp/x.x") >>= putStrLn ∘ toCostring ∘ g
                                 where
                                 g : String → String
                                 g str = str
-}                            

