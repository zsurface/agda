open import Data.Nat
open import Data.Nat.Show using (show)
open import Data.Unit using (⊤)
open import Data.String using (toCostring; String)
open import Foreign.Haskell using (Unit)
open import IO
import IO.Primitive as Prim

main : Prim.IO ⊤
main = do 
       run putStrLn (show 3)
