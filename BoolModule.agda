module BoolModule where

data Bool : Set where
  true : Bool
  false : Bool

not : Bool -> Bool
not true = false
not false = true

if_then_else_ : Bool -> Bool -> Bool -> Bool
if true then x else y = x
if false then x else y = y

_&&_ : Bool -> Bool -> Bool
true && b = b
fasle && b = false
