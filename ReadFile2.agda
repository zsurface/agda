open import Function using (_∘_)
open import Foreign.Haskell
open import IO.Primitive
open import Data.String using (String; toCostring)

main = (readFiniteFile "/tmp/x.x") >>= putStrLn ∘ toCostring ∘ g
       where
       g : String -> String
       g str = str 
