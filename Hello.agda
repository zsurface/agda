{-# OPTIONS --guardedness #-}
{-
{-# BUILTIN NATURAL Nat #-}
-}

module Hello where

data N : Set where
    zero : N
    suc : N → N

data Bool : Set where
  true : Bool
  false : Bool

not : Bool → Bool
not true = false
not false = true

_+_ : N → N → N
zero + zero = zero
zero + n = n
(suc n) + n' = suc (n + n') 

{-
  1*3 = 3 + zero
  2*3 = 3 + 3 + zero
  (x + 1)*3 = x*3 + 3
  
  suc n * m = (n + 1)*m = (m * n) + m
  suc zero * m = (m * zero) + m = m
  suc (suc zero) * m =  (m * (suc zero)) + m = m + m
  
   
-}
_*_ : N -> N -> N
zero * n = zero
(suc zero) * n = n
(suc m) * n = m + (m * n)   

_&&_ : Bool → Bool → Bool
b && false = false
b && true = b

_||_ : Bool → Bool → Bool
a || true = a
a || false = false

_=>_ : Bool → Bool → Bool
false => false = true



true => true = true
false => true = true
true => false = false

_<=>_ : Bool → Bool → Bool
true <=> true = true
false <=> false = true
true <=> false = false
false <=> true = false

{-
data even : n → set where
    zero : zero even
    step : ∀ x → x even → suc (suc x) even

prof1 : suc (suc (suc (suc zero))) even
prof1 = step _ (step _ zero)
-}

{- C-c C-x C-c => GHC RET -}



open import Agda.Builtin.IO
main = run (putStrLn "Hello1")     
